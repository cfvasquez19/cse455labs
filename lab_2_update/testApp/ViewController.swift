//
//  ViewController.swift
//  testApp
//
//  Created by Cesar Vasquez on 1/23/17.
//  Copyright © 2017 CSE455. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
   
    @IBOutlet weak var tittle: UILabel!
    
    @IBOutlet weak var shutDownLable: UILabel!
    @IBAction func shutdownButton(_ sender: AnyObject) {
        shutDownLable.text = "PRESS and HOLD iPhone power button to shutdown iPhone!"
    }
    
    @IBOutlet weak var restartLable: UILabel!
    @IBAction func restartButton(_ sender: AnyObject) {
        restartLable.text = "SORRY, iPhones do not have a restart function!"
    }
    
    @IBOutlet weak var sleepLable: UILabel!
    @IBAction func sleepButton(_ sender: AnyObject) {
        sleepLable.text = "Phones Dont Sleep, they just rest, LOL!!!"
    }

    @IBAction func exitButton(_ sender: AnyObject) {
        exit(0);
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

