//
//  ViewController.swift
//  Lab3
//
//  Created by Cesar Vasquez on 1/27/17.
//  Copyright © 2017 cse455. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController {

    @IBOutlet weak var output: UITextView!
    @IBOutlet weak var input: UITextField!
    @IBAction func tapButton(_ sender: AnyObject) {
        getString(var1: input.text!) { (result) in
            DispatchQueue.main.async(execute:{
                self.output.text = result
            })
        }
    }
    
    func getString(var1: String,  completion: @escaping (_ result: String)->Void)->Void{
        let myURL = URL(string: "http://date.jsontest.com")
        var request1 = URLRequest(url: myURL!)
        request1.httpMethod = "GET"
        let getString = "thing1=\(var1)" 
        request1.httpBody = getString.data(using: String.Encoding.utf8)
        let session = URLSession.shared
        
        let task = session.dataTask(with: request1, completionHandler:
            {(data: Data?, response: URLResponse?, error:Error? ) in
            if error != nil
            {
                print("error \(error)")
                return
            }
            else
            {
                if data != nil
                {
                    do
                    {
                        let myJson = try JSONSerialization.jsonObject(with: data!, options: []) as AnyObject
                        
                        if let date = myJson["date"] as? String
                        {
                            DispatchQueue.main.async(){self.output.text = date}
                        }
                        else
                        {
                            let time = myJson["time"] as? String
                            print(time)
                        }
                    }
                 
            
                }
            
            }
        } as! (Data?, URLResponse?, Error?) -> Void)
        task.resume()
        
        }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}



